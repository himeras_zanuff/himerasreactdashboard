from django.shortcuts import render

# Create your views here.
from django.shortcuts import render
from rest_framework import viewsets          # add this
from .serializers import EventSerializer      # add this
from .models import Event                     # add this

class EventView(viewsets.ModelViewSet):       # add this
    serializer_class = EventSerializer          # add this
    queryset = Event.objects.all()              # add this