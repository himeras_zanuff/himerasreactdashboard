import {
  createStore,
  applyMiddleware,
  compose
} from 'redux'
import {
  connectRouter,
  routerMiddleware
} from 'connected-react-router'
import thunk from 'redux-thunk';

import {
  createLogger
} from 'redux-logger';
import createHistory from 'history/createBrowserHistory'
import rootReducer from './reducers/rootReducer'
const loggerMiddleware = createLogger();
export const history = createHistory()

const initialState = {}
const enhancers = []
const middleware = [thunk, routerMiddleware(history)]

if (process.env.NODE_ENV === 'development') {
  const devToolsExtension = window.__REDUX_DEVTOOLS_EXTENSION__

  if (typeof devToolsExtension === 'function') {
    enhancers.push(devToolsExtension())
  }
}

const composedEnhancers = compose(
  applyMiddleware(...middleware,
    // thunkMiddleware,
    loggerMiddleware),
  ...enhancers
)

export default createStore(
  connectRouter(history)(rootReducer),
  initialState,
  composedEnhancers
)