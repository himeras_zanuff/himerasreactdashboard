import Dashboard from "../containers/Dashboard.js";
import Register from "../containers/Register.js"
import LoginView from "../containers/Login.js";
const indexRoutes = [
    {
        path :"/login/",
        component: LoginView
    },
    {
        path :"/register/",
        component: Register
    },
    {
        path :"/dashboard/",
        component: Dashboard
    },
];

export default indexRoutes;