import { connect } from "react-redux";
import EventManagement from "../components/EventManagement";

const mapStateToProps = state => {
	
};

const mapDispatchToProps = {
	// onLogin: authActions.login
};

export default connect(
	mapStateToProps,
	mapDispatchToProps
)(EventManagement);
