import { connect } from "react-redux";
import Dashboard from "../components/Dashboard";

const mapStateToProps = state => {
	
};

const mapDispatchToProps = {
	// onLogin: authActions.login
};

export default connect(
	mapStateToProps,
	mapDispatchToProps
)(Dashboard);
