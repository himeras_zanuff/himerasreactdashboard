import { connect } from "react-redux";
import RegisterForm from "../components/Register";

const mapStateToProps = state => {
};

const mapDispatchToProps = {
	// onLogin: authActions.login
};

export default connect(
	mapStateToProps,
	mapDispatchToProps
)(RegisterForm);
