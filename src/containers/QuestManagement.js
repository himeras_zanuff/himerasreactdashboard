import { connect } from "react-redux";
import QuestManagement from "../components/QuestManagement";

const mapStateToProps = state => {
	
};

const mapDispatchToProps = {
	// onLogin: authActions.login
};

export default connect(
	mapStateToProps,
	mapDispatchToProps
)(QuestManagement);
