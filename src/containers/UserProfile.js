import { connect } from "react-redux";
import UserProfile from "../components/UserProfile";
import { updateProfile } from "../actions/userActions";

const mapStateToProps = state => {
	return {
		userInfo: state.user
	}
};

const mapDispatchToProps = {
	onUpdateUserProfile: updateProfile
};

export default connect(
	mapStateToProps,
	mapDispatchToProps
)(UserProfile);
