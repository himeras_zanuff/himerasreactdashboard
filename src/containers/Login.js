import { connect } from "react-redux";
import LoginForm from "../components/Login";

const mapStateToProps = state => {
};

const mapDispatchToProps = {
	onLogin: authActions.login
};

export default connect(
	mapStateToProps,
	mapDispatchToProps
)(LoginForm);