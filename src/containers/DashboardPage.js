import { connect } from "react-redux";
import DashboardPage from "../components/DashboardPage";

const mapStateToProps = state => {
	
};

const mapDispatchToProps = {
	// onLogin: authActions.login
};

export default connect(
	mapStateToProps,
	mapDispatchToProps
)(DashboardPage);
