const API_URL = "https://jsonplaceholder.typicode.com"

const makeCallPost = (url, data) => {
    return fetch(url, {
        method: "POST",
        headers: {
            Accept: "application/json",
            "Content-Type": "application/json"
        },
        body: JSON.stringify(data)
    });
};


let userApi = {
    updateUserProfile: (data) => {
        return makeCallPost(API_URL + "/posts", data);
    },
};
export default {
    userApi: userApi
}