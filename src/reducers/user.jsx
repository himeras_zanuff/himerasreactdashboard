import { UPDATE_PROFILE } from "../actions/types";

let defaultState = {
        username: 'Ela',
        email: '',
        firstName: '',
        lastName: '',
        city: '',
        country: '',
        zipCode: '',
        about: ''
  };

  export default function (state = defaultState, action) {
    switch (action.type) {
      case UPDATE_PROFILE:
      console.log(state,action)
        return {
          ...state,
          ...action.data
        };
      default:
        return state;
    }
  }