import { combineReducers } from "redux";
import loginReducer from "./loginReducer";
import user from "./user";


const rootReducer = combineReducers({
  loginReducer,
  user
});

export default rootReducer