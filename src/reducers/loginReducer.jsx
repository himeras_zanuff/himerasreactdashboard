import {
  REGISTER,
  LOGIN,
  
} from "../actions/types";
  let defaultState = {
    isLoggedIn: undefined,
    token: localStorage.getItem('token'),
    errorMsg: null,
    loginData: {},
    needEmailConfirmation: null
  };
  
  
  export default function (state = defaultState, action) {
    switch (action.type) {
      case REGISTER:
        return {
          ...state,
          ...action.data
        }
      default:
        return state;
    }
  }