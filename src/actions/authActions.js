import { LOGIN } from "./types";
import api from '../api.js';


export const updateProfile = (userData) => (dispatch, getState) => {
    api.userApi.updateUserProfile(userData)
        .then(response => response.json(),
            error => console.log('An error occurred.', error)
            .then(jsonData =>
                dispatch({
                    type: LOGIN,
                    data: jsonData
                })
            ))
}