import { UPDATE_PROFILE } from "./types";
import api from '../api.js';

export const updateProfile = (userData) => (dispatch, getState) => {
    api.userApi.updateUserProfile(userData)
        .then(response => response.json(),
            error => console.log('An error occurred.', error)
            .then(jsonData =>
                dispatch({
                    type: UPDATE_PROFILE,
                    data: jsonData
                })
            ))
}

// export const register = (username,password) => (dispatch,getState) =>{
//     Api.auth.register({
//       email: username,
//       username: username,
//       password: password
//     }).then(function (response) {
//       return Promise.all([response, response.json()]);
//     }).then(function([response,jsonData]){
//       if(response.status)
//       dispatch({
//         type: REGISTER_START,
//         data:{
//           needEmailConfirmation: true
//         }
//       })
//     })
//   }


