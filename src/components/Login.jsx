import React, { Component } from "react";
// import {
//   Grid,
//   FormGroup,
//   ControlLabel,
//   FormControl
// } from "react-bootstrap";

import {Container, Row, Col } from 'reactstrap';
// import {connect} from 'react-redux'
// import * as authActions from '../actions/auth'
import Card from "components/Card/Card.jsx";
import CardHeader from "components/Card/CardHeader.jsx";
import CardBody from "components/Card/CardBody.jsx";
import CardFooter from "components/Card/CardFooter.jsx";
import Button from "./CustomButtons/Button";
import CustomInput from "./CustomInput/CustomInput";
import withStyles from "@material-ui/core/styles/withStyles";
import dashboardStyle from "assets/jss/material-dashboard-react/views/dashboardStyle.jsx";

class LoginForm extends Component {
    constructor(props) {
    super(props);
    this.state = {
      cardHidden: true,
      user: "placeholder",
      password: ""
    };
    this.onLogin = this.onLogin.bind(this)
    this.updateState = this.updateState.bind(this)
  }
  

  onLogin = (e) => {
    this.props.onLogin(this.state.user, this.state.password)
    return e.preventDefault();
  }

  updateState(propName) {
    return (e) => {
      this.setState({[propName]: e.target.value})
    }
  }

  render() {
  const { classes } = this.props;

    return (
      <div className="main-content">
          <Container>
          <Col md={{size:6, offset:3} }>
            <Card>
            <CardHeader color="primary">
              <h4 className={classes.cardTitleWhite}>Login</h4>
            </CardHeader>
              <CardBody>
              <CustomInput
                    labelText="Username"
                    id="username"
                    formControlProps={{
                      fullWidth: true
                    }}
                    inputProps={{
                      disabled: false
                    }}
                  />
                  <CustomInput
                    labelText="Password"
                    id="password"
                    formControlProps={{
                      fullWidth: true
                    }}
                    inputProps={{
                      disabled: false,
                    type:"password"

                    }}
                  />

              </CardBody>
              <CardFooter stats>
              <Container>
                <Row>
                    
                <Button block>Login</Button>
                    </Row>
                    <Row>
                    <Col md={{size:6, offset:3}} >
                    <span >
                      Don't have an account yet?
                    </span>
                    <a href="/register">   Sign Up</a>
                    
                    </Col></Row>
                    <Row>
                      <Col md={{size:8, offset:2}} >
                    <span >
                     Forgot your password?   
                    </span>
                    <a href="/register">   Recover your password</a>
                    </Col>

                    </Row>

              </Container>
                  
                  </CardFooter>
            </Card>
          </Col>
          
 
        </Container>
      </div>
    );
  }
  }

export default withStyles(dashboardStyle)(LoginForm);