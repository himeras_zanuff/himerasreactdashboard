import React,{
    Component
  } from 'react'
  import Board from 'react-trello'
  import { ToastContainer, toast } from 'react-toastify';
  import 'react-toastify/dist/ReactToastify.css';
  import Dialog from '@material-ui/core/Dialog';
  import DialogActions from '@material-ui/core/DialogActions';
  import DialogContent from '@material-ui/core/DialogContent';
  import DialogContentText from '@material-ui/core/DialogContentText';
  import DialogTitle from '@material-ui/core/DialogTitle';
  import Button from '@material-ui/core/Button';
  import Tabs from "components/CustomTabs/CustomTabs";
  import BugReport from "@material-ui/icons/BugReport";
  import Code from "@material-ui/icons/Code";
  import Cloud from "@material-ui/icons/Cloud";
  const data = {
    lanes: [
      {
        id: 'TODO',
        title: 'Assigned',
        label: '2/2',
        style: {background: "rgba(100,100,200,0.9)", color: "rgb(200,200,200)"},
        cards: [
          {id: 'Q_1', title: 'Serial Killer', description: 'Kill 3 players in 1 game', label: 'Fortnite'},
          {id: 'Q_2', title: 'Pacifist', description: 'Win a game Without killing any other player. You must wait until everyone is killed to win', label: 'Fortnite'}
        ]
      },
      {
        id: 'FINISHED',
        title: 'Completed',
        style: {background: "rgba(50,200,50,0.8)"},
        label: '0/0',
        cards: []
      }
    ]
  }
  const data1 = {
    lanes: [
      {
        id: 'TODO',
        title: 'Assigned',
        label: '2/2',
        style: {background: "rgba(100,100,200,0.9)", color: "rgb(200,200,200)"},
        cards: [
          {id: 'Q_1', title: 'Serial Killer', description: 'Kill 3 players in 1 game', label: 'Fortnite'},
        ]
      },
      {
        id: 'FINISHED',
        title: 'Completed',
        style: {background: "rgba(50,200,50,0.8)"},
        label: '0/0',
        cards: [
          {id: 'Q_2', title: 'Pacifist', description: 'Win a game Without killing any other player. You must wait until everyone is killed to win', label: 'Fortnite'}
        ]
      }
    ]
  }
  const data2 = {
    lanes: [
      {
        id: 'TODO',
        title: 'Assigned',
        label: '2/2',
        style: {background: "rgba(100,100,200,0.9)", color: "rgb(200,200,200)"},
        cards: [
        ]
      },
      {
        id: 'FINISHED',
        title: 'Completed',
        style: {background: "rgba(50,200,50,0.8)"},
        label: '0/0',
        cards: []
      }
    ]
  }
  const boardStyle={
      "background-color":"transparent"
  }
  class QuestManagement extends Component {
    constructor(props){
        super(props);
        this.state={
           open: false,
           dialog: { 
             content:'',
             title:''
           }
        }
        this.endDrag = this.endDrag.bind(this);
        this.onCardClick =this.onCardClick.bind(this);
    }

  
    handleClose = () => {
      this.setState({ open: false });
    };

    onCardClick= (cardId, metadata, laneId)=>{
      // toast("cardID:"+cardId+"Metadata: "+ metadata+"LaneID: "+ laneId);
      this.setState({ open: true });
      // this.state.dialog.content = cardId;
      // this.state.dialog.title = laneId;
      
    }
    endDrag = (cardId, sourceLaneId, targetLaneId) => {
      toast("cardID:"+cardId+"sourceLaneId: "+ sourceLaneId+"targetLaneId: "+ targetLaneId);
        return null
    }

    componentWillMount() {
    }
  
    render() {
        return (<div>

           <Tabs
      title="Tasks:"
      headerColor="rose"
      tabs={[
        {
          tabName: "Fortnite",
          tabIcon: BugReport,
          tabContent: (
           
          <Board draggable handleDragEnd={this.endDrag} onCardClick={this.onCardClick} style={boardStyle} data={data} />
          )
        },
        {
          tabName: "PUBG",
          tabIcon: Code,
          tabContent: (
            <Board draggable handleDragEnd={this.endDrag} onCardClick={this.onCardClick} style={boardStyle} data={data} />
  
          )
        },
        {
          tabName: "Server",
          tabIcon: Cloud,
          tabContent: (
<Board draggable handleDragEnd={this.endDrag} onCardClick={this.onCardClick} style={boardStyle} data={data} />
         
          )
        }
      ]}
    />


 
          <ToastContainer />
          <Dialog
          open={this.state.open}
          onClose={this.handleClose}
          aria-labelledby="alert-dialog-title"
          aria-describedby="alert-dialog-description"
        >
          <DialogTitle id="alert-dialog-title">{"Use Google's location service?"}</DialogTitle>
          <DialogContent>
            <DialogContentText id="alert-dialog-description">
              {this.state.dialog.content}
            </DialogContentText>
          </DialogContent>
          <DialogActions>
            <Button onClick={this.handleClose} color="primary">
              Cancel
            </Button>
            <Button onClick={this.handleClose} color="success" autoFocus>
              Mark as completed
            </Button>
          </DialogActions>
        </Dialog></div>)
    }
  }
  
  export default QuestManagement;