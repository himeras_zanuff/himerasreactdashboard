import React, { Component } from "react";
import {Container,  Col } from 'reactstrap';
import Card from "components/Card/Card.jsx";
import CardHeader from "components/Card/CardHeader.jsx";
import CardBody from "components/Card/CardBody.jsx";
import CardFooter from "components/Card/CardFooter.jsx";
import Button from "./CustomButtons/Button";
import CustomInput from "./CustomInput/CustomInput";
import withStyles from "@material-ui/core/styles/withStyles";
import dashboardStyle from "assets/jss/material-dashboard-react/views/dashboardStyle.jsx";

class RegisterForm extends Component {
    constructor(props) {
    super(props);
    this.state = {
      cardHidden: true,
      user: "placeholder",
      password: ""
    };
    this.onLogin = this.onLogin.bind(this)
    this.updateState = this.updateState.bind(this)
  }
  

  onLogin = (e) => {
    this.props.onLogin(this.state.user, this.state.password)
    return e.preventDefault();
  }

  updateState(propName) {
    return (e) => {
      this.setState({[propName]: e.target.value})
    }
  }

  render() {
  const { classes } = this.props;

    return (
      <div className="main-content">
          <Container>
          <Col md={{size:6, offset:3} }>
            <Card>
            <CardHeader color="success">
              <h4 className={classes.cardTitleWhite}>Register</h4>
            </CardHeader>
              <CardBody>
              <CustomInput
                    labelText="Username"
                    id="username"
                    formControlProps={{
                      fullWidth: true
                    }}
                    inputProps={{
                      disabled: false
                    }}
                  />
                   <CustomInput
                    labelText="Email"
                    id="email"
                    formControlProps={{
                      fullWidth: true
                    }}
                    inputProps={{
                    }}
                  />
                  <CustomInput
                    labelText="Password"
                    id="password"
                    formControlProps={{
                      fullWidth: true
                    }}
                    inputProps={{
                      disabled: false,
                    type:"password"

                    }}
                  />
                   <CustomInput
                    labelText="Confirm Password"
                    id="confirmPassword"
                    formControlProps={{
                      fullWidth: true
                    }}
                    inputProps={{
                      disabled: false,
                    type:"confirmPassword"

                    }}
                  />

              </CardBody>
              <CardFooter stats>
              <Button >Register</Button>
                  </CardFooter>
            </Card>
          </Col>
          
 
        </Container>
      </div>
    );
  }
  }

export default withStyles(dashboardStyle)(RegisterForm);