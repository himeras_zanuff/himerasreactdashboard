import React from "react";
import { render } from "react-dom";
import { Provider } from "react-redux";
import { ConnectedRouter } from 'connected-react-router'
import store, { history } from "./store";
import { Route, Switch } from 'react-router'
// import App from "./containers/App"; 
// import Activate from "./containers/activate";
import "bootstrap/dist/css/bootstrap.min.css";
import "./assets/sass/light-bootstrap-dashboard.css";
import "./assets/css/demo.css";
import "./assets/css/material-dashboard-react.css?v=1.4.1";
import indexRoutes from "routes/index.jsx";

// import "./assets/css/pe-icon-7-stroke.css";

import image from "./assets/img/full_screen.jpg";
const bgImage = {
	backgroundImage: "url(" + image + ") ",
	// background: "rgba(12,12,12,0.5)",
	resizeMode: 'cover',
	backgroundSize: 'cover',
	overflow: 'hidden',
	/* Set rules to fill background */
	minHeight: "100%",
	width: "100%",
	height: "auto",
	position: "fixed",
	top: "0",
	left: "0"
}
const target = document.querySelector("#root");
render(
	<Provider store={store}>
		<ConnectedRouter history={history}>
			<div style={bgImage}>
			<Switch>
			{indexRoutes.map((prop, key) => {
        return <Route path={prop.path} component={prop.component} key={key} />;
      })}
				{/*<Route path="/activate/:uid/:token" component={Activate}/> */}
				{/* <Route path="/" component={App} /> */}
			</Switch>
			</div>
		</ConnectedRouter>
	</Provider>,
	target
);
